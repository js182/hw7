function filterBy(arr, typeDate) {
  const newArr = [];

  arr.forEach((el) => {
    if (typeof el !== typeDate) {
      newArr.push(el);
    }
  });

  return newArr;
}

const data = ["hello", "world", 23, "23", null];
const type = "string";

console.log(filterBy(data, type));
